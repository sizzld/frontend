import React from 'react'
import './AvailableGames.css'

function AvailableGames() {
    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-12 mt-4'>
                    <h3 className='page_title'>Available Games</h3>
                </div>
                <div className='col-md-12 mt-4'>
                    <div className='games_card'>
                        <div className='row'>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-one.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-two.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-three.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-four.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-five.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-six.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-seven.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-eight.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-one.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-two.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-three.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-3 mt-3'>
                                <div class="card individual_game_card">
                                    <img src="/game-four.png" class="card-img-top w-100" alt="..." />
                                    <div class="card-body p-2">
                                        <p class="card-text position-relative">35,000 Player <br /> Playing Now
                                            <div className='redirect'>
                                                <img src="/arrow.png" alt="" />
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AvailableGames